import React from 'react';
import './todoItem.css';
import Checkbox from '@material-ui/core/Checkbox';

export default class TodoItem extends React.Component{
    constructor(props){
        super(props);
    }
    removeTodo(id){
        this.props.removeTodo(id);
    }
    editTodo(id){
        this.props.editTodo(id);
    }

    render(){
        return(
            <div className= "todoWrapper">
                <Checkbox /><button className= "removeTodo" onClick={(e)=>this.removeTodo(this.props.id)}>Remove</button>{this.props.todo.text}<button className= "removeTodo" onClick={(e)=>this.editTodo(this.props.id)}>Edit</button>
            </div>
        );  
    }
}